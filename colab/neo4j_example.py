#!/usr/python

from neo4j import GraphDatabase

# Create a class to execute NEO4j queries
class Neo4jDriver:
    def __init__(this, uri='bolt://0.0.0.0', user=None, password=None):
      this.driver = GraphDatabase.driver(uri, auth=(user, password))
    def close(this):
      this.driver.close()
      
    def executequery(this, query, parameters=None, db=None):
      session = this.driver.session(database=db) if db is not None else this.driver.session()
      response = []
      try:
        response = list(session.run(query, parameters))
      except Exception as e:
        print("Query failed:", e)
      finally: 
        if session is not None:
          session.close()
        return response

# setup connection
uri = "bolt://0.0.0.0:7687"
user = "neo4j"
password = "Password!"
driver = Neo4jDriver(uri,user,password)

# check plugins
query = """
RETURN gds.similarity.jaccard(
  [1.0, 5.0, 3.0, 6.7],
  [5.0, 2.5, 3.1, 9.0]
) AS jaccardSimilarity
"""
print(driver.executequery(query))

query = "CALL gds.list()"
print(driver.executequery(query))

query = """
RETURN date(apoc.date.format(timestamp(),'ms', 'yyyy-MM-dd'))
"""
result = driver.executequery(query)
for record in result:
  print(record[0])

# get test data
# https://github.com/mneedham/graphing-brexit/tree/master/data/commonsvotes

data = """
UNWIND [655,656,657,658,659,660,661,662,711, 669, 668, 667, 666, 664] AS division
LOAD CSV FROM "https://github.com/mneedham/graphing-brexit/raw/master/data/commonsvotes/Division" + division + ".csv" AS row
// Create motion nodes
WITH division, collect(row) AS rows
MERGE (motion:Motion {division: trim(split(rows[0][0], ":")[1]) })
SET motion.name = rows[2][0], 
    motion.date = date(datetime({epochmillis:apoc.date.parse(trim(split(rows[1][0], ":")[1]), "ms", "dd/MM/yyyy")}))
// Skip the first 6 rows as they have metadata we don't need
WITH motion, rows
UNWIND rows[7..] AS row
// Create person, party, constituency, and corresponding rels
MERGE (person:Person {name: row[0]})
MERGE (constituency:Constituency {name: row[2]})
MERGE (person)-[:REPRESENTS]->(constituency)
WITH person, motion,  
     CASE WHEN row[3] = "Aye" THEN "FOR" 
          WHEN row[3] = "No" THEN "AGAINST" 
          ELSE "DID_NOT_VOTE" END AS vote
CALL apoc.merge.relationship(person, vote, {}, {}, motion)
YIELD rel
RETURN count(*)
"""
result = driver.executequery(data)

# query Person entity
query = """
MATCH (p:Person) RETURN p;
"""
person = driver.executequery(query)
for p in person:
  print(p)

# sample data
query =  "UNWIND range(1, 3) AS n RETURN n, n * n as n_sq"
result = driver.executequery(query)

# using py2neo
!pip install py2neo

# read test data from parquet file
# reading from a parquet file
import pandas as pd
df_test = pd.read_parquet("/content/pet_test.parquet")

# insert data into neo4j using pandas dataframe
# adding data into Neo4j
from py2neo import Graph
graph = Graph("bolt://172.28.0.12:7687")
tx = graph.begin()
for index, row in df_test.iterrows():
  tx.evaluate('''
       MERGE (p:Pet {petid:$petid}) ON CREATE
       SET p.type = $type,
      p.age = $age,
      p.breed = $breed,
      p.gender = $gender,
      p.maturitysize  = $maturitysize
       MERGE (t:Type {type:$type})
       MERGE (g:Gender {gender:$gender})
       MERGE (p)-[r:BREEDTYPE]->(t)
       MERGE (p)-[r1:GENDERTYPE]->(g)
       ''', parameters = {'petid': index,
                          'type':row['Type'],
                          'age':row['Age'],
                          'breed':row['Breed1'],
                          'gender':row['Gender'],
                          'maturitysize':row['MaturitySize']})
tx.commit()\

# query the database
q='''
MATCH (p)-[:GENDERTYPE]->(g)
RETURN p, g LIMIT 5
'''
for a in graph.run(q):
  print(a)


