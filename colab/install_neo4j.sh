#!/bin/bash
wget -O - https://debian.neo4j.com/neotechnology.gpg.key | sudo apt-key add -
echo 'deb https://debian.neo4j.com stable latest' | sudo tee /etc/apt/sources.list.d/neo4j.list
sudo apt-get update
apt install neo4j
# update default password using
neo4j-admin dbms set-initial-password "Password!"
# mv apoc jar under plugins and set neo4j.conf directives
mv /var/lib/neo4j/labs/apoc-5.14.0-core.jar /var/lib/neo4j/plugins/
# download gds
wget https://github.com/neo4j/graph-data-science/releases/download/2.5.5/neo4j-graph-data-science-2.5.5.jar
!mv neo4j-graph-data-science-2.5.5.jar /var/lib/neo4j/plugins/
mv /var/lib/neo4j/labs/apoc-5.14.0-core.jar /var/lib/neo4j/plugins/

pip install neo4j
# edit /etc/neo4j/neo4j.conf and then restart neo4j
# line: 67 -> server.default_listen_address=0.0.0.0 (uncomment)
# line 207 -> dbms.security.allow_csv_import_from_file_urls=true (uncomment)
# line 241 -> dbms.security.procedures.unrestricted=apoc.*,gds.*
# line 243 -> dbms.security.procedures.unrestricted=apoc.*,gds.*

# line 27 - for disabling authentication
#sed -i  's/dbms.security.auth_enabled=true/dbms.security.auth_enabled=false/g' /etc/neo4j/neo4j.conf
# line 68
sed -i 's/#server.default_listen_address=0.0.0.0/server.default_listen_address=0.0.0.0/g' /etc/neo4j/neo4j.conf
# line 245
sed -i 's/dbms.security.procedures.allowlist=apoc.coll.*,apoc.load.*,gds.*/dbms.security.procedures.allowlist=apoc.*,gds.*,algo.*/g' /etc/neo4j/neo4j.conf

# run neo4j
neo4j start > /dev/null

