#!/usr/bin/env python
# coding: utf-8
import re
from pyspark.sql import functions as F
import utils.functions as lf
import utils.data_checks as dc
import utils.filename_handler as fh
from datetime import datetime

def cdc_sa(df_source,df_target):
  """
  parameters:
  df_source = source data to ingest 
  df_target = data already in HIVE table
  
  variables: asumes your datasets 
  id_col
  row_hash
  live_flag
  """  
  id_col = df_target.columns[0]
  df_no_changes = df_source.join(df_target,df_source[id_col] == df_target[id_col],"left")\
                    .where(df_source['row_hash'] == df_target['row_hash'])\
                    .select(df_source['*']).dropDuplicates()
  df_changes = df_source.join(df_target,df_source[id_col] == df_target[id_col],"inner")\
                  .where(df_source['row_hash'] != df_target['row_hash'])\
                  .select(df_source['*'])
  df_changes = df_changes.withColumn('live_flag',F.lit(False))
  df_t = df_no_changes.union(df_changes)
  df_source_target = df_t.union(df_source)
  df_source_target.persist()
   
  return df_source_target

def get_filename(filepath,regex="\\.csv",latest=True):
  """
  get filename with full path
  i.e. regex = "BasicCompanyDataAsOneFile-"+filedate+"\\.csv"  
  """
  files = fh.SAFileHandler(filepath,regex).get_filepaths(True)
  if latest:
    return files[-1]
  else:
    return files

def is_file_loaded(df,filename,col="source_filename"):
  """
  check if is loaded or not
  you may also use df.limit(1).count() == 1
  """
  if (df.filter(F.col(col) == filename).rdd.isEmpty()):
    return False
  else:
    return True

def load_csv_file(spark,filename):
  """
  load CSV file
  """
  df = spark.read.option("header",True).csv(filename)
  # remove space from column name
  df = lf.rename_cols_regex(df)
  fname = filename.split("/")[-1]
  df = df.withColumn('live_flag',F.lit(True))\
             .withColumn('source_filename',F.lit(fname))\
             .withColumn("load_timestamp",F.lit(F.current_timestamp()))\
             .withColumn("row_hash",F.sha2(F.concat_ws("",*df.columns),256))
  return df

# load target data
def load_target_data(spark,targettable):
  """
  load data from table
  """
  df = spark.sql("""select * from {}""".format(targettable))
  df.cache().count()
  return df

def load_all_sa_data(filepath,regexp,logging):
  """
   read all data and resturn result as a dataframe 
   uses file_handler. SAFileHandler class
  """
  df_s = None
  files = fh.SAFileHandler(filepath,regexp).get_filepaths(True)
  for file in files:
    try:
      filename = filepath + file 
      df = load_sa_data(filename)
      if df_s is None:
        df_s = df
      else:
        df_s = df_s.unionAll(df)
    except:
      logging.warning('ignored ' + filename)
      print(filename)
      continue
  return df_s

def get_join(df_s,df_t,id_col):
  """
  join dataframes and return columns from both side - prefix column names with _s and _t
  warning - careful with large dataframes
  """
  df_s = df_s.select(*(F.col(x).alias(x + '_s') for x in df.columns))
  df_t = df_t.select(*(F.col(x).alias(x + '_t') for x in df1.columns))
  df_join = df_s.join(df_t,df_s[col + '_s'] == df_t[col+ '_t'],"inner")\
                .where(df_s['row_hash_s'] != df_t['row_hash_t'])\
                .select('*')
  return df_join

# get sa data from database
def get_data_from_db(spark,tablename,col_value= '2022',col_name='source_filename'):
  """get data from target table
  parameters:
  spark
  tablename
  col_value
  col_name
  """
  df = spark.sql(f"""select * from {tablename} where {col_name} rlike '{col_value}'""")
  return df

# save data as parquet file
def save_data(spark,df,targettable):
  """save data as parquet tble
  """
  df.createOrReplaceTempView('tmp_table')
  stmt = f"""SELECT * FROM tmp_table"""
  spark.sql(f"""CREATE TABLE IF NOT EXISTS {targettable} STORED AS PARQUET AS {stmt} WHERE 1<>1""")
  spark.sql(f"""INSERT OVERWRITE TABLE {targettable} {stmt}""")
   
  return True  

# save process history changes
def save_change_history(spark,df,pchtable,id_col):
  """ save data changes in a table
  parameters:
  df
  pchttable
  id_col
  assuming data changes are captured in 'data_changes' column and using soource_filename column for date
  """
  df = df.filter(F.trim(df.data_changes) != '')
  df.createOrReplaceTempView('pch')
  stmt = f"""
SELECT 
{id_col},
trim(data_changes) AS pch_type,
regexp_extract(source_filename,"([0-9]+)",1) file_date,
concat('Q',quarter(to_date(regexp_extract(source_filename,"([0-9]+)",1),'yyyyMMdd'))) as download_quarter,
year(to_date(regexp_extract(source_filename,"([0-9]+)",1),'yyyyMMdd')) as download_year,
reflect('java.util.UUID','randomUUID') as pch_guid,
live_flag
FROM pch"""
  # save data as PARQUET table
  spark.sql("""CREATE TABLE IF NOT EXISTS {} STORED AS PARQUET AS {} WHERE 1<>1""".format(pchtable,stmt))
  spark.sql("""INSERT INTO TABLE {} {}""".format(pchtable,stmt))
 
  return True

# process change history
def find_changes(spark,logging,df_source_exists,cols):
  """identify changes
  """
  change_col = "data_changes"
  col = df_source_exists.columns[0]
  logging.info('Using ID column: ' + col)
  print(col)
  df_changes = df_source_exists.withColumn(change_col,F.lit(""))
 
  logging.info('Tracking changes for:')
  loop_count = 0
  for col in cols:
    logging.info(col)
    print(col)
    loop_count += 1
    df_changes = df_changes.withColumn(change_col, dc.add_to_string_col(
      df_changes[change_col],f"{col}",
      dc.check_changes(df_changes[col],df_changes[col+'_x'])))
    # incase if you want to avoid over writing previous non null values
    df_changes = df_changes.withColumn(col,F.coalesce(df_changes[col],df_changes[col+'_x']))
    if loop_count %4 == 0:
      df_changes = df_changes.persist()
      
  return df_changes

# process and save changes
def process_save_changes(spark,logging,df_source,df_target,change_cols,targettable=None,pchtable=None):
  """identify changes in two dataframes
  parameters:
  df_source - dataframe containing new dataset
  df_target - existing datasets (target dataframe will updated accordingly)
  change_cols - list of varibles to for capturing chnages    
  """
  logging.info('START - identify changes: {}'.format(datetime.now().strftime("%Y%m%d%H%M")))
  print('START - identify changes: {}'.format(datetime.now().strftime("%Y%m%d%H%M")))
  change_col = "data_changes"
  col = df_source.columns[0]
  
  # separate target data by live_flag
  df_active = df_target.filter(df_target['live_flag'] == True) 
  df_inactive = df_target.filter(df_target['live_flag'] == False) 
  # cache dataframes
  #print(f"Active records: {df_active.cache().count()}")
  #print(f"Inactive records: {df_inactive.cache().count()}")
  #print(f"Source: {df_source.cache().count()}")

  # records already exist
  df_t = df_active.select(*(F.col(x).alias(x + '_x') for x in df_active.columns))
  df_amends = df_source.join(df_t,df_source[col] == df_t[col+'_x'],'inner')\
                           .select('*')\
                           .dropDuplicates([col]) 
  #print(f"Amends: {df_amends.cache().count()}")
  df_amends = df_amends.persist()

  #  no changes
  df_no_changes = df_active.join(df_source,df_active[col] == df_source[col],"left")\
                .where(df_source[col].isNull())\
                .select(df_active['*'])
  #print(f"No changes: {df_no_changes.cache().count()}")
  df_no_changes = df_no_changes.persist()

  # to keep history
  df_amends_prev = df_amends.select(df_t['*'])
  df_amends_prev = lf.rename_cols(df_amends_prev,'_x','')
  df_amends_prev = df_amends_prev.withColumn("live_flag",F.lit(False))
  df_target_updated = df_no_changes.union(df_amends_prev)
  df_target_updated = df_target_updated.union(df_inactive)

  # join dataframes to create an updated target dataframe  without history
  #df_target_updated = df_no_changes.union(df_inactive)
  #df_target_updated.cache().count()

  # birth
  df_birth = df_source.join(df_target, on=[col], how='leftanti')
  df_birth = df_birth.withColumn(change_col,F.lit(2))
  print(f"Birth: {df_birth.cache().count()}")
  
  # death
  df_death = df_target.join(df_source, on=[col], how='leftanti')
  df_death = df_death.withColumn(change_col,F.lit(99))
  print(f"Death: {df_death.cache().count()}")
  
  # identify updated records
  if (df_amends.rdd.isEmpty()): 
    df_changes = df_amends
    df_changes = df_changes.withColumn(change_col,F.lit(""))
  else:
    df_changes = find_changes(spark,logging,df_amends,change_cols)
  df_changes = df_changes.drop(*df_t.columns)
  
  # join new and updated source records
  df_changes_birth = df_birth.union(df_changes)
  df_all_changes = df_changes_birth.union(df_death)
  #print(f"All changes: {df_all_changes.cache().count()}")

  # save change history
  if pchtable is not None:
    save_change_history(spark,df_all_changes,pchtable,col)
  
  # join source and target
  df_changes = df_changes_birth.drop(change_col)
  df_source_target = df_changes.union(df_target_updated)
  #print(f"All records: {df_source_target.cache().count()}")
  
  # save data
  if targettable is not None:
    save_sa_data(spark,df_source_target,targettable)

  logging.info('END - identify changes: {}'.format(datetime.now().strftime("%Y%m%d%H%M")))
  print('END - identify changes: {}'.format(datetime.now().strftime("%Y%m%d%H%M")))
                             
  return df_source_target, df_all_changes

# implementation of CDC using dataframes
def change_data_capture(df_source,df_target,id_col=None,change_cols=None):
  if id_col is None:
    id_col = df_target.columns[:1]
  if change_cols is None:
    change_cols = df_target.columns    
  # 0=no change, 2=New, 3=change and 99=delete  
  conditions = [F.when(df_source[c] != df_target[c], F.lit(c)).otherwise("") for c in change_cols if c not in [id_col]]
  change_type = (F.when(df_target[id_col].isNull(), F.lit(2))
          .when(df_source[id_col].isNull(), F.lit(99))
          .when(F.size(F.array_remove(F.array(*conditions), "")) > 0, F.lit(3))
          .otherwise(0)
         )
  select_expr =[ 
                *[df_source[c].alias(c) for c in df_source.columns if c not in change_cols],
                *[F.coalesce(df_source[c], df_target[c]).alias(c) for c in change_cols if c not in [id_col]],                
                F.array_remove(F.array(*conditions), "").alias("data_changes"),
                change_type.alias("change_type"),
  ]
  df_out = df_source.join(df_target, [df_target.id_col==df_source.id_col],how="outer").select(*select_expr)
  return df_out

# delta records
def generate_delta(df_source,df_target,id_col=None,change_cols=None):
  if id_col is None:
    id_col = df_target.columns[:1]
  if change_cols is None:
    change_cols = df_target.columns
  # delta
  select_expr =[
    *[F.coalesce(crn[c], crn_s[c]).alias(c) for c in crn.columns if c not in change_cols],
    *[F.coalesce(crn_s[c], crn[c]).alias(c) for c in change_cols if c not in [id_col]],
  ]
  df_out = df_source.join(df_target, [df_source[id_col]==df_target[id_col]],how="outer").select(*select_expr)
  return df_out

def identify_changes(spark,logging,df_source,df_target,id_col=None,change_cols=None,tablename='cdc',changetable='cdc_changes'):
  """ performing CDC and saving changes to PRQUET partioned table
  partition_key - live_flag (int 0=false and 1=true) 
  id_col - to perform outer join
  change_cols = tracking changes for given variables
  tblename - parquet table name for saving data
  changetable - table for change history (only append data into the table - partition key - loaddate)

  - source dataframe - split them into 
      - birth (only present in the source) 
      - death (compare last load data with source and record any missing records) 
      - amends
  - current records dataframe - which are not present in the source data (only get data where live_flag=1)
  """
  logging.info('START - identify changes: {}'.format(datetime.now().strftime("%Y%m%d%H%M")))
  print('START - identify changes: {}'.format(datetime.now().strftime("%Y%m%d%H%M")))

  if id_col is None:
    id_col = df_source.columns[:1]
  if change_cols is None:
    change_cols = df_source.columns

  # identify births, deaths and changes - outer join with broadcast
  df_t = df_target.select(*(F.col(x).alias(x + '_x') for x in df_target.columns))
  source_target = (df_t.join(F.broadcast(df_source),df_source[id_col] == df_t[id_col+'_x'],'outer')
                        .withColumn('cdc',F.when(F.col(id_col+'_x').isNull(), F.lit(2))
                                           .when(F.col(id_col).isNull(), F.lit(99))
                                           .otherwise(F.lit(3)))
                        .select('*')
              )
  source_target = source_target.persist()

  # births
  births = source_target.filter(F.col(id_col+'_x').isNull()).select(df_source.columns)
  # no_changes)
  no_changes = source_target.filter(F.col(id_col).isNull()).select(df_t.columns)
  no_changes = lf.rename_cols(no_changes,'_x','')
  output = births.union(no_changes).drop('live_flag')

  # apply data changes
  conditions = [F.when(source_target[c] != source_target[c+'_x'], F.lit(c)).otherwise(F.lit("")) for c in change_cols if c not in [id_col]]
  select_expr = [
    *[F.coalesce(source_target[c+'_x'], source_target[c]).alias(c) for c in df_source.columns if c not in change_cols],
    *[F.coalesce(source_target[c], source_target[c+'_x']).alias(c) for c in change_cols if c not in [id_col]],
    F.array_remove(F.array(*conditions), "").alias("data_changes")
  ]
  changes = source_target.filter(source_target.cdc==3).select(*select_expr,'cdc')
 
  # save change history as parquet table
  changes.write.partitionBy('loaddate').mode('append').parquet(changetable)
  (source_target.filter(source_target.cdc!=3)
    .select(*df_source.columns,F.array(F.lit("")).alias('data_changes'),'cdc')
    .write.partitionBy('loaddate')
    .mode('append').parquet(changetable)
  )
  # save data
  changes = changes.select(*df_source.columns).drop('live_flag')
  output = changes.union(output)
  
  # overwrite live_flag=1
  output = output.withColumn('live_flag',F.lit(1))
  output.filter(output.live_flag==1).write.partitionBy('live_flag') \
    .mode('overwrite').option("partitionOverwriteMode", "dynamic").parquet(tablename)
  # need to keep history  
  old_changes = source_target.filter(source_target.cdc==3).select(*df_t.columns)
  old_changes = lf.rename_cols(old_changes,'_x','')
  old_changes = old_changes.withColumn('live_flag',F.lit(0))
  old_changes.filter(old_changes.live_flag==0).write.partitionBy('live_flag').mode('append').parquet(tablename)

  logging.info('END - identify changes: {}'.format(datetime.now().strftime("%Y%m%d%H%M")))
  print('END - identify changes: {}'.format(datetime.now().strftime("%Y%m%d%H%M")))

  return output

# mapping Companies House variables
def ch_mapping(spark,df):
  """create a temporary table and rename variables
  """
  cols = 'PreviousName_1CompanyName,PreviousName_2CompanyName,PreviousName_3CompanyName,\
        PreviousName_4CompanyName,PreviousName_5CompanyName,PreviousName_6CompanyName,\
        PreviousName_7CompanyName,PreviousName_8CompanyName,PreviousName_9CompanyName,PreviousName_10CompanyName'
  col_dates = 'PreviousName_1CONDATE,PreviousName_2CONDATE,PreviousName_3CONDATE,\
        PreviousName_4CONDATE,PreviousName_5CONDATE,PreviousName_6CONDATE,\
        PreviousName_7CONDATE,PreviousName_8CONDATE,PreviousName_9CONDATE,PreviousName_10CONDATE'
  address = 'RegAddressCareOf,RegAddressPOBox,RegAddressAddressLine1,RegAddressAddressLine2,\
        RegAddressPostTown,RegAddressCounty,RegAddressCountry'

  df.createOrReplaceTempView("companieshouse")  

  sql_stmt = f"""
SELECT CompanyNumber,CompanyName,
CompanyCategory,CompanyStatus,AccountsAccountCategory,
concat_ws(',',{address}) AS RegAddress,
RegAddressPostCode,CountryOfOrigin,
from_unixtime(unix_timestamp(IncorporationDate,'dd/MM/yyyy')) AS IncorporationDate,
from_unixtime(unix_timestamp(AccountsNextDueDate,'dd/MM/yyyy')) AS AccountsNextDueDate,
from_unixtime(unix_timestamp(AccountsLastMadeUpDate,'dd/MM/yyyy')) AS AccountsLastMadeUpDate,
from_unixtime(unix_timestamp(ReturnsNextDueDate,'dd/MM/yyyy')) AS ReturnsNextDueDate,
from_unixtime(unix_timestamp(ReturnsLastMadeUpDate,'dd/MM/yyyy')) AS ReturnsLastMadeUpDate,
CASE
  WHEN (length(SICCodeSicText_1) = 0 ) THEN NULL
  ELSE trim(split(SICCodeSicText_1,'-')[0])
END AS SICCode_1,
trim(split(SICCodeSicText_1,'-')[1]) AS SICDescription_1,
CASE
  WHEN (length(SICCodeSicText_2) = 0 ) THEN NULL
  ELSE trim(split(SICCodeSicText_2,'-')[0])
END AS SICCode_2,
trim(split(SICCodeSicText_2,'-')[1]) AS SICDescription_2,
CASE
  WHEN (length(SICCodeSicText_3) = 0 ) THEN NULL
  ELSE trim(split(SICCodeSicText_3,'-')[0])
END AS SICCode_3,
trim(split(SICCodeSicText_3,'-')[1]) AS SICDescription_3,
CASE
  WHEN (length(SICCodeSicText_4) = 0 ) THEN NULL
  ELSE trim(split(SICCodeSicText_4,'-')[0])
END AS SICCode_4,
trim(split(SICCodeSicText_4,'-')[1]) AS SICDescription_4,
concat_ws(',',{cols}) as PreviousNameCompanyName,
concat_ws(',',{col_dates}) as PreviousNameCONDATE
FROM companieshouse
"""
  df_ch = spark.sql(sql_stmt)
  
  return df_ch


