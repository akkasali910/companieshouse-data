#!/usr/bin/env python
# coding: utf-8

# ## Introduction
# In this notebook, we will implemet CDC using PySpark. All the functions are included in this note book together with test data.
# 
# ### Change Data Capture Implementation 
# * target dataframe (table containing historical and current data)
# * source dataframe (incoming file)
# * identify changes in variables and update the target table
#     * separate target data by live_flag
#     * records already exist
#     * no changes
#     * join dataframes to create an updated target dataframe with history
#     * dataframe to hold records exist in the incoming file
#     * dataframe to hold records does not exist in the incoming file - looking at target table
#     * dataframe storing updated records
#     * save change histroy
#     * join dataframes and update target table
# 

# In[1]:


from pyspark.sql import SparkSession
from pyspark import SparkContext
from pyspark.sql import functions as F
from pyspark.sql import types as T
from datetime import datetime
import logging

spark = SparkSession.builder\
       .config("spark.sql.warehouse.dir","./spark-warehouse") \
       .appName('pyspark-cdc')\
       .enableHiveSupport()\
       .getOrCreate()


# In[2]:


logfile = './log/compare.txt'


# In[3]:


from logging.handlers import TimedRotatingFileHandler
import logging
import os
def set_log_to_file(log_path):
  """Set logging output to a given file path. Logging is done using 
  a time rotatinckup logs are retained. Logging statements of INFO. or higher are logged.
  
  :param log_path: path to log. Patrh is created if it is not exist.
  :type log_path: String
  """
  
  try:
    os.makedirs("/".join(log_path.split("/")[:-1]))
  except FileExistsError:
    pass
  
  logging.basicConfig(
    format="%(asctime)s^%(levelname)s^%(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
    level=logging.INFO,
    handlers=[TimedRotatingFileHandler(log_path, when="W0", backupCount=5)])


# In[4]:


set_log_to_file(logfile)


# In[5]:


# save process history changes
def save_change_history(spark,df,pchtable):
  #df = df.filter(F.trim(df.data_changes) != '')
  col = df.columns[0]
  df.createOrReplaceTempView('pch')
  stmt = f"""
SELECT 
{col}
,trim(data_changes) AS pch_type
,load_timestamp
,reflect('java.util.UUID','randomUUID') as pch_guid
,live_flag
FROM pch"""
  # save data as PARQUET table
  try:
    spark.sql("""INSERT INTO TABLE {} {}""".format(pchtable,stmt))
  except:
    spark.sql("""CREATE TABLE IF NOT EXISTS {} STORED AS PARQUET AS {}""".format(pchtable,stmt))  
 
  return True


# In[6]:


# save data as parquet file
def save_target_data(spark,df,targettable):
  df.createOrReplaceTempView('sa_2020')
  sql_stmt = f"""SELECT * FROM sa_2020"""
  # save data into a table
  try:
    spark.sql("""INSERT OVERWRITE TABLE {} {}""".format(targettable,sql_stmt))
  except:
    spark.sql("""CREATE TABLE IF NOT EXISTS {} STORED AS PARQUET AS {}""".format(targettable,sql_stmt))  
 
  return True  


# In[7]:


# get sa data from database
def get_target_data(spark,targettable,live_flag=True):
  spark = SparkSession.builder.getOrCreate()  
  df = spark.sql("""select * from {} where live_flag = {}""".format(targettable,live_flag))
  df.persist()
  return df


# In[8]:


def add_metadata(df,file='Derived'):
  df = df.withColumn('live_flag',F.lit(True))\
         .withColumn('filename',F.lit(file))\
         .withColumn("load_timestamp",F.lit(F.current_timestamp()))\
         .withColumn("row_hash",F.sha2(F.concat_ws("",*df.columns),256))
  return df


# In[9]:


def rename_cols(df,char,newchar):
  for column in df.columns:
    new_column = column.replace(char,newchar)
    df = df.withColumnRenamed(column, new_column)
  return df


# In[10]:


def rename_cols_append(df,newchar):
  df = df.select(*(F.col(x).alias(x + newchar) for x in df.columns))
  return df


# In[11]:


def check_changes(col1,col2):
  compare_cols = (col1 == col2)
  return ((col1.isNull()) & (col2.isNull())) | compare_cols


# In[12]:


def add_to_string_col(col,fail_string,check_col):
  """
  append a string to error_col
  """
  fail_text = (F.when((check_col == True), "")
               .otherwise(F.when(col == "", fail_string)
                          .otherwise(f",{fail_string}")))
  return F.concat(col, fail_text)


# In[13]:


# process change history
def find_changes(spark,logging,df_source_exists,cols):
  """identify changes
  """
  change_col = "data_changes"
  col = df_source_exists.columns[0]
  logging.info('Using ID column: ' + col)
  print(col)
  df_changes = df_source_exists.withColumn(change_col,F.lit(""))
 
  logging.info('Tracking changes for:')
  loop_count = 0
  for col in cols:
    logging.info(col)
    print(col)
    loop_count += 1
    df_changes = df_changes.withColumn(change_col, add_to_string_col(
      df_changes[change_col],f"{col}",
      check_changes(df_changes[col],df_changes[col+'_t'])))
    # incase if you want to avoid over writing previous non null values
    # df_changes = df_changes.withColumn(col,F.coalesce(df_changes[col],df_changes[col+'_t']))
    if loop_count %4 == 0:
      df_changes.cache().count()
      
  return df_changes

# identify and save changes
def process_save_changes(spark,logging,df_source,df_target,change_cols,targettable=None,pchtable=None):

  """identify changes in two dataframes
  parameters:
  df_source - dataframe containing new dataset
  df_target - existing datasets (target dataframe will updated accordingly)
  change_cols - list of varibles to for capturing chnages    
  """
  logging.info('START - identify changes: {}'.format(datetime.now().strftime("%Y%m%d%H%M")))
  print('START - identify changes: {}'.format(datetime.now().strftime("%Y%m%d%H%M")))
  change_col = "data_changes"
  col = df_source.columns[0]
  
  # separate target data by live_flag
  df_active = df_target.filter(df_target['live_flag'] == True) 
  df_inactive = df_target.filter(df_target['live_flag'] == False) 
  # cache dataframes
  print(f"Active records: {df_active.cache().count()}")
  print(f"Inactive records: {df_inactive.cache().count()}")
  print(f"Source: {df_source.cache().count()}")

  # records already exist
  df_t = df_active.select(*(F.col(x).alias(x + '_t') for x in df_active.columns))
  df_amends = df_source.join(df_t,df_source[col] == df_t[col+'_t'],'inner')\
                           .select('*')\
                           .dropDuplicates([col]) 
  print(f"Amends: {df_amends.cache().count()}")
  
  #  no changes
  df_no_changes = df_active.join(df_source,df_active[col] == df_source[col],"left")\
                .where(df_source[col].isNull())\
                .select(df_active['*'])
  print(f"No changes: {df_no_changes.cache().count()}")

  # to keep history
  df_amends_prev = df_amends.select(df_t['*'])
  df_amends_prev = rename_cols(df_amends_prev,'_t','')
  df_amends_prev = df_amends_prev.withColumn("live_flag",F.lit(False))
  df_target_updated = df_no_changes.union(df_amends_prev)
  df_target_updated = df_target_updated.union(df_inactive)

  # join dataframes to create an updated target dataframe  without history
  #df_target_updated = df_no_changes.union(df_inactive)
  #df_target_updated.cache().count()

  # birth
  df_birth = df_source.join(df_target, on=[col], how='leftanti')
  df_birth = df_birth.withColumn(change_col,F.lit(2))
  print(f"Birth: {df_birth.cache().count()}")
  
  # death
  df_death = df_target.join(df_source, on=[col], how='leftanti')
  df_death = df_death.withColumn(change_col,F.lit(99))
  print(f"Death: {df_death.cache().count()}")
  
  # identify updated records
  if (df_amends.rdd.isEmpty()): 
    df_changes = df_amends
    df_changes = df_changes.withColumn(change_col,F.lit(""))
  else:
    df_changes = find_changes(spark,logging,df_amends,change_cols)
  df_changes = df_changes.drop(*df_t.columns)
  
  # join new and updated source records
  df_changes_birth = df_birth.union(df_changes)
  df_all_changes = df_changes_birth.union(df_death)
  print(f"All changes: {df_all_changes.cache().count()}")

  # save change history
  if pchtable is not None:
    save_change_history(spark,df_all_changes,pchtable,col)
  
  # join source and target
  df_changes = df_changes_birth.drop(change_col)
  df_source_target = df_changes.union(df_target_updated)
  print(f"All records: {df_source_target.cache().count()}")
  
  # save data
  if targettable is not None:
    save_sa_data(spark,df_source_target,targettable)

  logging.info('END - identify changes: {}'.format(datetime.now().strftime("%Y%m%d%H%M")))
  print('END - identify changes: {}'.format(datetime.now().strftime("%Y%m%d%H%M")))
                             
  return df_source_target, df_all_changes

def process_save_changes(spark,logging,df_source,df_target,change_cols,targettable=None,pchtable=None):
  """
  identify changes in two dataframes
  
  parameters:
    df_source - dataframe containing new dataset
    df_target - existing datasets (target dataframe will updated accordingly)
    change_cols - list of varibles to for capturing chnages    
  """
  logging.info('START - identify changes: {}'.format(datetime.now().strftime("%Y%m%d%H%M")))
  print('START - identify changes: {}'.format(datetime.now().strftime("%Y%m%d%H%M")))
  change_col = "data_changes"
  col = df_source.columns[0]
  
  # separate target data by live_flag
  df_active = df_target.filter(df_target['live_flag'] == True) 
  df_inactive = df_target.filter(df_target['live_flag'] == False) 
  df_active.cache().count()
  df_inactive.cache().count()
  df_source.cache().count()

  # records already exist
  df_t = df_active.select(*(F.col(x).alias(x + '_t') for x in df_active.columns))
  df_amends = df_source.join(df_t,df_source[col] == df_t[col+'_t'],'inner')\
                           .select('*')\
                           .dropDuplicates([col]) 
  df_amends.cache().count()
  
  #  no changes
  df_no_changes = df_active.join(df_source,df_active[col] == df_source[col],"left")\
                .where(df_source[col].isNull())\
                .select(df_active['*'])

  # to keep history
  df_amends_prev = df_amends.select(df_t['*'])
  df_amends_prev = rename_cols(df_amends_prev,'_t','')
  df_amends_prev = df_amends_prev.withColumn("live_flag",F.lit(False))
  df_target_updated = df_no_changes.union(df_amends_prev)
  df_target_updated = df_target_updated.union(df_inactive)

  # join dataframes to create an updated target dataframe  without history
  #df_target_updated = df_no_changes.union(df_inactive)
  #df_target_updated.cache().count()

  # birth
  df_birth = df_source.join(df_target, on=[col], how='leftanti')
  df_birth = df_birth.withColumn(change_col,F.lit(2))
  df_birth.cache().count()
  
  # death
  df_death = df_target.join(df_source, on=[col], how='leftanti')
  df_death = df_death.withColumn(change_col,F.lit(99))
  df_death.cache().count()
  
  # identify updated records
  df_changes = find_changes(spark,logging,df_amends,change_cols)
  df_changes = df_changes.drop(*df_t.columns)
  
  # join new and updated source records
  df_all_changes = df_birth.union(df_changes)
  df_all_changes = df_all_changes.union(df_death)
  
  # save change history
  if pchtable is not None:
    save_change_history(spark,df_all_changes,pchtable)
      
  # join source and target
  df_changes = df_all_changes.drop(change_col)
  df_source_target = df_changes.union(df_target_updated)
  
  # save data
  if targettable is not None:
    save_target_data(spark,df_source_target,targettable)

  logging.info('END - identify changes: {}'.format(datetime.now().strftime("%Y%m%d%H%M")))
  print('END - identify changes: {}'.format(datetime.now().strftime("%Y%m%d%H%M")))
                             
  return df_source_target, df_all_changes


# In[14]:


# sample data = create two dataframes
# df_target = master or history data frame
# df_source = current or data to added into target
df_target = spark.createDataFrame([(11,'Sam',1000,'ind','IT','2/11/2019'),(22,'Tom',2000,'usa','HR','2/11/2019'),
                                 (33,'Kom',3500,'uk','IT','2/11/2019'),(44,'Nom',4000,'can','HR','2/11/2019'),
                                 (55,'Vom',5000,'mex','IT','2/11/2019'),(66,'XYZ',5000,'mex','IT','2/11/2019')],
                                 ['Id','Name','Sal','Address','Dept','Join_Date']) 
df_source = spark.createDataFrame([(11,'Sam',1000,'ind','IT','2/11/2019'),(22,'Tom',2000,'usa','HR','2/11/2019'),
                                  (33,'Kom',3000,'GB','MKT','12/12/2019'),(44,'Nom',4000,'can','HR','2/11/2019'),
                                  (45,'Xom',5000,'mex','IT','2/11/2019'),(77,'XYZ',5000,'mex','IT','2/11/2019')],
                                  ['Id','Name','Sal','Address','Dept','Join_Date']) 


# In[15]:


df_target.show()


# In[16]:


df_source.show()


# In[17]:


target = add_metadata(df_target)
source = add_metadata(df_source)


# In[18]:


target.show(truncate=False)


# In[19]:


# initial data load
targettable='cdc'
pchtable='cdc_pch'
# initial data load
save_target_data(spark,df_target,targettable)


# In[20]:


cols = target.columns[1:-4]


# In[21]:


pchtable='cdc_pch'
df = process_save_changes(spark,logging,source,target,cols,targettable,pchtable)


# In[22]:


df


# In[23]:


cols = df[0].columns[:-3]
df[0].sort('Id').select(cols).show()


# In[24]:


cols = cols + ['data_changes']


# In[25]:


df[1].sort('Id').select(cols).show()


# In[28]:


df[0].show(vertical=True)


# In[29]:


df3 = spark.sql('select * from cdc_pch')


# In[30]:


df3.show(truncate=False,vertical=True)


# In[31]:


spark.stop()


# In[ ]:




