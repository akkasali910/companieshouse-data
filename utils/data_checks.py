import datetime
from pyspark.sql.column import Column
from pyspark.sql import functions as F
from pyspark.sql import types as T
import logging

def assert_column(col):
  try:
    assert type(col) == Column
  except AssertError:
    raise(TypeError("Function expects a column."))

def check_date_range(col,max_date,date_format="YYYY-MM-dd",min_date=datetime.date(2020,3,3)):
  assert_column(col)
  date_col = F.to_date(col,date_format)
  date_in_range = (date_col >= min_date) & (date_col <= max_date)
  return (date_col.isNotNull() & date_in_range) | col.isNull()

def check_date(col,date_format = "YYYY-MM-dd"):
  assert_column(col)
  date_col = F.to_date(col,date_format)
  return (date_col.isNotNull()) | col.isNull()

def check_integer(col):
  assert_column(col)
  numeric_col = col.cast("float")
  int_col= col.cast("int")
  return col.isNull() | (int_col.isNotNull() & (int_col == numeric_col))

def check_numeric(col):
  assert_column(col)
  return col.isNull() | col.cast("float").isNotNull()

def check_positive_number(col):
  assert_column(col)
  numeric_col = col.cast("float")
  return col.isNull() | (numeric_col.isNotNull() & (numeric_col > 0.0))

def check_specific_values(col, *values):
  assert_column(col)
  check_values = col.isin([value for value in values if value is not None])
  if None in values:
    return (check_values) | (col.isNull())
  else:
    return (check_values) &  (col.isNotNull())

def check_regex(col, pattern, null_accepted=False):
  if null_accepted == True:
    return col.rlike(f"^{pattern}$") | (col.isNull())
  else:
    return col.rlike(f"^{pattern}$")

def check_not_null(col):
   return col.isNotNull()

def check_not_blank(col):
    return_code = (F.length(F.trim(col)).cast('int'))	
    return return_code > 0


def check_changes(col1,col2):
  compare_cols = (col1 == col2)
  return ((col1.isNull()) & (col2.isNull())) | compare_cols

def check_postcode(col):
  """
  Checks that values in a pyspark column are of UK postcode format or null.
  True does not indicate correct postcodes, only verify format.
  
  args
    col: (pyspark.sql.column.Column) Column containing the data to test.
    
  returns
    A pyspark boolean column, which is True where the input is valid postcode format.
  """
  assert_column(col)
  postcode_regex = "^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|"\
                     "(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9][A-Za-z]?))))\\s?[0-9][A-Za-z]{2})$"
  return (col.rlike(postcode_regex)) | col.isNull()

def add_to_string_col(col,fail_string,check_col):
  """
  append a string to error_col
  """
  fail_text = (F.when((check_col == True), "")
               .otherwise(F.when(col == "", fail_string)
                          .otherwise(f",{fail_string}")))
  return F.concat(col, fail_text)

def column_iterator(col_list,  df, error_warning_col):
  """
  column iterator
  """
  def decorator(func):
    def wrapper(*args, **kwargs):
      nonlocal df
      output_string=kwargs["output_string"]
      kwargs.pop("output_string")
      loop_count = 0
      for col in col_list:
        logging_message = kwargs.get("logging_message", None)
        if logging_message is not None:
          logging.info(logging_message.replace("{col}",col))
          kwargs.pop("logging_message")
        bool_col = func(df[col], *args, **kwargs)
        df = df.withColumn(
          error_warning_col,
          add_to_string_col(df[error_warning_col],
                            f"{output_string}({col})",
                            bool_col))
        loop_count += 1
        if loop_count%3 == 0:
          df.cache().count()
      return df
    return wrapper
  return decorator

def run_checks(df):
    
  error_col = "data_errors"
  df = df.withColumn(error_col,F.lit(""))
    
  def add_to_string_col(col,fail_string,check_col):
    """
    append a string to error_col
    """
    fail_text = (F.when((check_col == True), "")
                 .otherwise(F.when(col == "", fail_string)
                            .otherwise(f",{fail_string}")))
    return F.concat(col, fail_text)

  date_check_cols = [
      "payment_date"
  ]
    
  for col in date_check_cols:
    df = df.withColumn(error_col, add_to_string_col(
      df[error_col], f"check_date({col})",
      check_date(df[col])))
    df.cache().count()

  for col in date_check_cols:
    df = df.withColumn(error_col, add_to_string_col(
      df[error_col], f"check_date_range({col})",
      check_date_range(df[col],f"2021-08-01",f"2020-08-02")))
    df.cache().count()
    
  int_check_cols = [
      "id"
  ]
    
  for col in int_check_cols:
    df = df.withColumn(error_col, add_to_string_col(
      df[error_col], f"check_integer({col})",
      check_integer(df[col])))
    df.cache().count()
    
  return df  

#process change history
def find_changes(spark,df_source,df_source_exists,id_col,cols):
  change_col = "data_changes"
  print(col)  
  #identify updated records
  df_t = df_source_exists.select(*(F.col(x).alias(x + '_t') for x in df_source_exists.columns))
  df_changes = df_source.join(df_t,df_source[id_col] == df_t[id_col+'_t'],"inner")\
                .select('*').dropDuplicates(['id_col'])
  df_changes = df_changes.withColumn(change_col,F.lit(""))
    
  i = 0
  for col in cols:
    print(col) 
    i=i+1
    df_changes = df_changes.withColumn(change_col, add_to_string_col(
      df_changes[change_col],f"{col}",
      check_changes(df_changes[col],df_changes[col+'_t'])))
    # incase if you want to avoid over writing previous non null values
    #df_changes = df_changes.withColumn(col,F.coalesce(df_changes[col],df_changes[col+'_t']))
    if (i%4 == 0):
      df_changes.cache().count()

  #drop columns
  df_changes = df_changes.drop(*df_t.columns)
  
  return df_changes
