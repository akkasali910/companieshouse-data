#!/bin/bash
# elasticsearch
wget -O elasticsearch-8.11.1-linux-x86_64.tar.gz 'https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-8.11.1-linux-x86_64.tar.gz'
tar -xf elasticsearch-8.11.1-linux-x86_64.tar.gz
sudo chown -R daemon:daemon elasticsearch-8.11.1/

# needed for elasticssearch
umount /sys/fs/cgroup
apt install cgroup-tools

# python es driver
pip install elasticsearch

# edit Enable security features - set it to false
# xpack.security.enabled: true

# run eelasticsearch
sudo -H -u daemon elasticsearch-8.11.1/bin/elasticsearch > /dev/null &
