"""
download datasets from know sources 
 - companies hoiuse
 - chrity commission
"""
#!/usr/bin/env python
# coding: utf-8

# https://download.companieshouse.gov.uk/en_pscdata.html
def get_ch_psc_date(date_str='2023-09-30'):
  !wget https://download.companieshouse.gov.uk/persons-with-significant-control-snapshot-{date_str}.zip
  !unzip persons-with-significant-control-snapshot-{date_str}.zip -d /tmp/
  return True

def get_charity_data(spark):
  !wget https://ccewuksprdoneregsadata1.blob.core.windows.net/data/txt/publicextract.charity.zip
  !unzip publicextract.charity.zip -d /tmp
  df_charity = spark.read.option("delimiter", "\\t").option("header", "true").csv("file:///tmp/publicextract.charity.txt")
  # save data to drive
  df_charity.coalesce(1).write.format("parquet").mode("append").save(f"file:///tmp/charity_data")
  return df_charity


