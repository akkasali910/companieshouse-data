##Notes
Run both elasticsearch and neo4j without login user id and password. For this you need to uncomment or edit few directives in config/elasticsearch.yml and conf/neo4j.conf respectively.

###Neo4j as a service
```
%%bash
wget -O - https://debian.neo4j.com/neotechnology.gpg.key | sudo apt-key add -
echo 'deb https://debian.neo4j.com stable latest' | sudo tee /etc/apt/sources.list.d/neo4j.list
sudo apt-get update
apt install neo4j 

# update default password using
!neo4j-admin dbms set-initial-password "pasword min 8 chars"

# mv apoc jar under plugins and set neo4j.conf directives

!mv /var/lib/neo4j/labs/apoc-5.14.0-core.jar /var/lib/neo4j/plugins/

# update neo4j.conf file - lines 240 and 244 respectively as:
## dbms.security.procedures.unrestricted=apoc.*
##dbms.security.procedures.unrestricted=apoc.*

```

###Neo4j
```
#uncomment the following directives
#dbms.security.auth_enabled=false
#server.default_listen_address=0.0.0.0
```
###Exposing ports using ngrok
Create an account and get authenticastion key from ngrok website
```
# replace with your own key
!ngrok authtoken {your_ky}
```
Configure ngrok with your own application specific ports
```
#using ngrok for exposing multipl ports
%%writefile /content/http.yml
version: "2"
tunnels:
  elasticsearch:
    proto: http
    addr: 9200
  neo4j:
    proto: http
    addr: 7474
  neo4jbolt:
    proto: tcp
    addr: 7687    
  streamlitapp:
    proto: http
    addr: 8501 
```
Use your config file to invoke ngrok
```
%%writefile /content/run_ngrok.sh
#!/bin/sh
set -x
ngrok start --config /root/.config/ngrok/ngrok.yml --config /content/http.yml --log=stdout "$@"

# run your script - see below
import os
get_ipython().system_raw('bash /content/run_ngrok.sh elasticsearch neo4j neo4jbolt &')
```
The bolt port on 7687 is not on http, but on TCP.
- use ngrok tcp 7687 instead
- also, in the neo4j connection config, you need to replace tcp:// syntax with bolt:// at the first URL, so neo4j will recognize the connection. It's like this:
  * bolt://tcp.ngrok.io:3000

###Eleasticsearch
```
# Enable security features - set it to false
xpack.security.enabled: true

see. https://discuss.elastic.co/t/curl-against-an-encrypted-elasticsearch-instance-with-certificate-verification/251503
```
