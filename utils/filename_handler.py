import os
import re

class FileNameHandler():
  """
  Class for handling file names in HDFS folder
  """
  
  def __init__(self,root_dir,filename_regex):
    """
    iniliaise with a root directory and a regex for indentifying filename.
    """
    self.root_dir = root_dir
    self.filename_regex = filename_regex
    
  def _get_all_hdfs_filepaths(self):
    """
    iniliaise with a root directory and a regex for indentifying filename.
    """
    cmd = "hdfs dfs -ls -R {}".format(self.root_dir) +  " | sed 's/ */ /g' | cut -d\ -f8"
    return [
      file[:-1] for file in os.popen(cmd).readlines()]

  def _get_all_filepaths(self):
    """
    iniliaise with a root directory and a regex for indentifying filename.
    """
    cmd = "ls -ls {}".format(self.root_dir) + " | awk '{print $10}'"
    return [
      file[:-1] for file in os.popen(cmd).readlines()]

  def _check_valid_filepath(self,filepath):
    """
    regex to check filepath.
    """
    pattern = self.filename_regex + "$"
    return re.search(pattern,filepath) is not None

  def get_hdfs_filepaths(self, return_all=False):
    """
    returns a list of hdfs filepath
    """
    filepaths = self._get_all_hdfs_filepaths()
    valid_paths = [
      path for path in filepaths if self._check_valid_filepath(path)  
    ]
    if return_all:
      return valid_paths
    else:
      valid_paths.sort()
      return valid_paths[-1:]
 
  def get_filepaths(self, return_all=False):
    """
    returns a list of filepath
    """
    filepaths = self._get_all_filepaths()
    valid_paths = [
      path for path in filepaths if self._check_valid_filepath(path)
    ]
    if return_all:
      return valid_paths
    else:
      valid_paths.sort()
      return valid_paths[-1:]

  def _regex_filepath(self, filepath):
    """
    regex to extract the year, month and filename from file path
    """
    m = re.findall("/(\\d{4}/(\\d{2}/v(\\d*)/("+self.filename_regex + ")$",filepath)[0]
    return {"year": m[0], "month": m[1], "version": m[2], "filename": m[3]}


class SAFileHandler(FileNameHandler):
  """
  File name handler for SA
  """
  def __init__(self, file_path, filename_regexp):
    self.root_dir = (f"{file_path}/")
    self.filename_regex = ((f"{filename_regexp}"))    
    
class CsvOutputHandler(FileNameHandler):
  """
  File name handler for output .csv
  """
  def __init__(self, csv_filepath):
    self.root_dir = (f"{csv_filepath}/")
    self.filename_regex = ("(.*)\.csv")

  def _check_valid_filepath(self,filepath):
    """
    regex to check filepath.
    """
    pattern = self.filename_regex + "$"
    return re.search(pattern,filepath) is not None

def get_filename(path, extension="",  drop_extension=True):
  try:
    assert path.endswith(extension)
  except AssertionError:
    raise AssertionError(f"{extension} does not match the end of the string {path}")
  
  pattern = re.compile("(\\|\/)*[^\\\/]+$")
  try:
    filename = pattern.search(path).group()  
  except AssertError:
    raise AssertionError(f"cannot find filename in {path}")
    
  if drop_extension and (extension != ""):
    filename = filename.split(extension)[0]
    
  return filename