#!/usr/bin/env python
# coding: utf-8
import re
import os
import logging
from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql import types as T
from datetime import datetime
from logging.handlers import TimedRotatingFileHandler

# create spark context
def spark(appname="PySparkETL",scala_version="2.12",spark_version="3.5.0"):
  """
  createa  spark session with kafka support
  """
  packages = [
    f'org.apache.spark:spark-sql-kafka-0-10_{scala_version}:{spark_version}',
    'org.apache.kafka:kafka-clients:3.6.1'
]
  spark = (SparkSession.builder
         .appName(appname)
         .config("spark.jars.packages", ",".join(packages))
         .getOrCreate()
  )
  return spark

#ngrok - needs pyngrok
def get_public_url(key,port=40411):
  """
  using pyngrok to get public URL
  prerequiste - must sign up GROK and obtain API key
  importp methods -  from pyngrok import ngrok, conf
  """
  if key is None:
    print("NGROK API key is required")
    return False
  conf.get_default().auth_token = key
  ui_port = port
  public_url = ngrok.connect(ui_port).public_url
  print(f" * ngrok tunnel \"{public_url}\" -> \"http://127.0.0.1:{ui_port}\"")
  return True

#logging
def set_logging_colab(logfile='app.log'):
  """
  Google colab - default log level is DBUG and does not show INFO
  workaround - remove handler(s)
  Use basicConfig to set log level
  """
  # clear previous handler(s)
  for handler in logging.root.handlers[:]:
    logging.root.removeHandler(handler)
		
  # set logging config
  logging.basicConfig(filename=logfile,
    filemode="w",
    format="%(asctime)s^%(levelname)s^%(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
    level=logging.INFO)	

def set_log_to_file(log_path):
  """Set logging output to a given file path. Logging is done using 
  a time rotatinckup logs are retained. Logging statements of INFO. or higher are logged.
  
  :param log_path: path to log. Patrh is created if it is not exist.
  :type log_path: String
  """
  
  try:
    os.makedirs("/".join(log_path.split("/")[:-1]))
  except FileExistsError:
    pass
  
  logging.basicConfig(
    format="%(asctime)s^%(levelname)s^%(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
    level=logging.INFO,
    handlers=[TimedRotatingFileHandler(log_path, when="W0", backupCount=1)])

def add_metadata(df,filename):
  df = df.withColumn('live_flag',F.lit(True))\
         .withColumn('source_filename',F.lit(filename))\
         .withColumn("load_timestamp",F.lit(F.current_timestamp()))\
         .withColumn("row_hash",F.sha2(F.concat_ws("",*df.columns),256))
  return df
        
def rename_cols(df,char,newchar):
  for column in df.columns:
    new_column = column.replace(char,newchar)
    df = df.withColumnRenamed(column, new_column)
  return df

def rename_cols_regex(df,newchar=""):
  for column in df.columns:
    new_column = re.sub(r"(\%)|(-)|(\.)|(\s)|(\()|(\))", newchar, column)
    df = df.withColumnRenamed(column, new_column)
  return df

def rename_cols_append(df,newchar):
  df = df.select(*(F.col(x).alias(x + newchar) for x in df.columns))
  return df

def get_all_changes(df,df1,col,cols):
  df_join = df.join(df1,df[col] == df1[col],"inner").select('*')
  df_changes = df_join.agg(*[F.count(F.when(df[c] != df1[c], c)).alias(c) for c in cols])
  return df_changes

def concat_cols(*cols):
  """
  Exclude empty columns while using concat_ws - use it select or dataframe transformation 
  """
  return F.trim(F.concat_ws('-', *[F.when(c != '', c) for c in cols]))

def clean_postcode(postcode=""):
  """
  just return postcode
  """
  return F.regexp_replace(F.upper(postcode), "[^a-zA-Z0-9,\s]","")

@F.udf(T.StringType())
def postcode_from_string(pc_string):
  """
  extract postcodefrom string
  """
  postcode_regex = "([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|"\
                     "(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9][A-Za-z]?))))\\s?[0-9][A-Za-z]{2})"
  return [x[0] for x in re.findall(f"((?<![a-zA-Z0-9])({postcode_regex})|({postcode_regex}$|{postcode_regex}\s))",pc_string)]

def null_values(df):
  df = df.agg(*[F.count(F.when(F.isnull(c), c)).alias(c) for c in df.columns])
  return df

def no_values(df):
  df = df.agg(*[F.count(F.when(F.trim(F.col(c)) == '', c)).alias(c) for c in df.columns])
  return df

def sanity_check(df):
  """
  quick QA checks for null values, min and max length for variables in a dataframe
  """
  df = df.agg(*[(F.count(F.when(F.isnull(c), c)).alias(c + '_null')) for c in df.columns],
                   *[(F.count(F.when(F.trim(F.col(c)) == '', c)).alias(c)) for c in df.columns],
                   *[(F.max(F.length(c))).alias(c + '_max') for c in df.columns],
                   *[(F.min(F.length(c))).alias(c + '_min') for c in df.columns],
                   *[(F.countDistinct(F.col(c)).alias(c + '_distinct')) for c in df.columns])
  return df

#join dataframes and resolve ambitious field names by remaning them
def join_df(df,df1,col):
  df_r = df.select(*(F.col(x).alias(x + '_df') for x in df.columns))
  df1_r = df1.select(*(F.col(x).alias(x + '_df1') for x in df1.columns))
  df_join = df_r.join(df1_r,df_r[col + '_df'] == df1_r[col+ '_df1'],"inner").select('*')
  return df_join

#get quarter
def get_quarter(d):
  """get quarter from date and rteturn as q_{quarter}
  """  
  return "q%d_%d" % (math.ceil(d.month/3), d.year)

# get charity data
def get_charity_data(spark):
  os.systm("wget https://ccewuksprdoneregsadata1.blob.core.windows.net/data/txt/publicextract.charity.zip")
  os.system("unzip publicextract.charity.zip -d /tmp")
  df_charity = spark.read.option("delimiter", "\\t").option("header", "true").csv("file:///tmp/publicextract.charity.txt")
  # save data to drive
  df_charity.coalesce(1).write.format("parquet").mode("append").save(f"file:///tmp/charity_data")
  return df_charity

#diagnostic information
def diag_groupby_output(spark,df,path,file):
  run_date_time = datetime.now().strftime("%Y%m%d%H%M")
  start_time = datetime.now()
  row_count = df.count()
  print('Diagnostic GroupBy started(limit 20 records): {}'.format(run_date_time))
  df.createOrReplaceTempView("diags")
  report_file = path + file + "_" + run_date_time + ".log"
  df_diag = None
  for col in list(df.columns):
    df = spark.sql(f"""SELECT c.{col}, {row_count} as total_rows,
                         c.col_count AS count,  round((c.col_count/{row_count}),2) as proportion
                       FROM (SELECT {col}, count(*) as col_count FROM diags
                         GROUP BY {col}) c 
                       ORDER BY 1 LIMIT 20""")
    
    
    print(df.show(truncate=False))
    #save data
    if df_diag is None:
      df_diag = df
    else:
      cols = df.columns
      values = [df.columns]
      df1 = spark.createDataFrame(values,cols)
      df_diag = df_diag.union(df1)
      df_diag = df_diag.union(df)      
  #write to a file
  try:
    os.makedirs(path)
  except FileExistsError:
    pass
  
  #dataframe to pandas dataframe                                 
  df = df_diag.toPandas()
  with open(report_file,'a') as ofile:
    df.to_csv(ofile, mode='a', index=False)
  
  end_time = datetime.now()
  print('Diagnostic Group By completed:  {}'.format(end_time - start_time))              
  return True

#print output
def diag_print_output(spark,df,limit=20):
  run_date_time = datetime.now().strftime("%Y%m%d%H%M")
  start_time = datetime.now()
  row_count = df.count()
  print('Diagnostic GroupBy started(limit {} records): {}'.format(limit,run_date_time))
  df.createOrReplaceTempView("diags")
  for col in list(df.columns):
    df = spark.sql(f"""SELECT c.{col}, {row_count} as total_rows,
                         c.col_count AS count, round((c.col_count/{row_count}),2) as proportion
                       FROM (SELECT {col}, count(*) as col_count FROM diags
                         GROUP BY {col}) c 
                       ORDER BY 2 DESC LIMIT {limit}""")
    print(df.show(truncate=False))
  
  end_time = datetime.now()
  print('Diagnostic Group By completed:  {}'.format(end_time - start_time))              
  return True
