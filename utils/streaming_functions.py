#!/usr/bin/env python
# coding: utf-8

# save schema
def save_schema(schema,filename):
  """
  need to import - json library
  """

  with open(filename, "w") as f:
    json.dump(schema.jsonValue(), f)
  return True

# read saved schema
def read_schema(filename):
  """
  need to import - StructType and json libraries
    - from pyspark.sql.types import StructType
  """
  with open(filename) as f:
    new_schema = StructType.fromJson(json.load(f))
  return new_schema

# read data from kafka (batch)
def kafka_read_data(spark,hosts,topic):
  """
  read data from kafaka topic
  """
  df = (spark
    .read
    .format("kafka")
    .option("kafka.bootstrap.servers", hosts)
    .option("group-id", "pyspark-group")
    .option("subscribe", topic)
    .option("startingOffsets", "earliest")
    .load()
    )
  df1 = df.select(F.col('key').cast('string'), 
              'topic', 'partition', 'offset', 'timestamp', 'timestampType',
              F.substring(F.col('timestamp'),0,10).alias('published_date'),
              F.col('value').cast('string')
          )

  return df1

#get data using starting and ending Offsets
def kafka_read_data_with_offsets(spark,hosts,topic,start,end):
  """
  start and end as json string - i.e. {"0":-2} or {"0":-1}
  """
  topic_start = '{"'+topic+'":'+start+'}'
  topic_end = '{"'+topic+'":'+end+'}'
  df = (spark
    .read
    .format("kafka")
    .option("kafka.bootstrap.servers", hosts)
    .option("subscribe", topic)
    .option("startingOffsets", topic_start)
    .option("endingOffsets", topic_end)
    .load()
    )
  df1 = df.select(F.col('key').cast('string'), 
              'topic', 'partition', 'offset', 'timestamp', 'timestampType',
              F.substring(F.col('timestamp'),0,10).alias('published_date'),
              F.col('value').cast('string')
          )
  return df1


# get all data
def kafka_readstream_data(spark,hosts,topic):
  df = (spark
    .readStream
    .format("kafka")
    .option("kafka.bootstrap.servers", hosts)
    .option("group-id", "pyspark-group")
    .option("subscribe", topic)
    .load()
    )
  df1 = df.select(F.col('key').cast('string'), 
              'topic', 'partition', 'offset', 'timestamp', 'timestampType',
              F.substring(F.col('timestamp'),0,10).alias('published_date'),
              F.col('value').cast('string')
          )

  return df1  

#write to kafka consumer (batch mode)
def kafka_write_data(df,hosts,topic):
  """
  write to Kafka topic
  1. convert dataframe to json string 
  2. use a composite keys for key column - monotonically_increasing_id + current_timestamp
  """
  (df
    .select(F.concat(F.monotonically_increasing_id(),F.lit('-'),F.current_timestamp()).alias('key'),
            F.to_json(F.struct([F.col(c).alias(c) for c in df.columns])).alias("value"))
    .write
    .format("kafka")
    .option("kafka.bootstrap.servers", hosts)
    .option("topic", topic)
    .save()
    )
  return True

#write to MySQL database
def mysql_write_data(df,conn_str,table_name):
  """
  write to MySQL database
  1. convert Spark dataframe to Pandas
  2. use pandas to_sql method for inserting data into the database
  """
  df = df.select(F.concat(F.monotonically_increasing_id(),F.lit('-'),F.current_timestamp()).alias('id'),
           F.to_json(F.struct([F.col(c).alias(c) for c in df.columns])).alias("payload"))

  pdf = df.toPandas()
  engine = create_engine(conn_str, echo=False)
  rc = pdf.to_sql(name=table_name, con=engine, if_exists = 'append', index=False)
  return rc

def deserialize_payload(spark,df,schema=None):
  """
  Deserialize payload from json string into Struct or Map type
  If schema is noty given then it will infere schema from the data
  """ 
  if schema is None:
    schema = spark.read.json(df.rdd.map(lambda row: row.value)).schema
  # create a new column and store exploded json values - make sure scheam is defined
  df1 = df.withColumn("payload", F.from_json("value", schema)).drop('value')
  return df1, schema    
